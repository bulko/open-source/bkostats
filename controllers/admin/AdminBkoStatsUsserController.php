<?php
/**
*2015 Bulko
*
*BkoStats module
*
*@author	Bulko (Golga)
*@copyright	Copyright (c) Bulko
*@license	Addons PrestaShop license
*
*AdminBkoStatsUsserController tab for admin panel
*/

include_once 'AdminBkoStatsController.php';

class AdminBkoStatsUsserController extends AdminBkoStatsController
{
	private $statOpt = array("Utilisateur activé", "Utilisateur inscrit");

	public function __construct()
	{
		parent::__construct( $this->statOpt );
		return true;
	}

	public function initContent()
	{
		parent::initContent();
		$this->displayStat();

		return true;
	}

	public function getNewUserByPeriod( $periodID = null )
	{
		$query = 'SELECT 
				count(' . _DB_PREFIX_ . 'customer.id_customer) AS "nb_user",
				SUM(' . _DB_PREFIX_ . 'customer.optin) AS "nb_optin",
				SUM(' . _DB_PREFIX_ . 'customer.newsletter) AS "nb_newsletter"
				FROM ' . _DB_PREFIX_ . 'customer 
				WHERE 1';
		if( isset($periodID) && $periodID != "all" )
		{
			$monthStr = parent::getValidMonth( $this->period[$periodID]["month"] );
			$query .= " AND " . _DB_PREFIX_ . "customer.date_add >= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-00 00:00:00'";
			$query .= " AND " . _DB_PREFIX_ . "customer.date_add <= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-32 00:00:00'";
		}
		$this->lastQuery = $query;
		$this->lastStat = Db::getInstance()->executeS( $query );

		return $this->lastStat;
	}

	public function getNewActifUserByPeriod( $periodID = null )
	{
		$query = 'SELECT 
				count(' . _DB_PREFIX_ . 'customer.id_customer) AS "nb_user",
				SUM(' . _DB_PREFIX_ . 'customer.optin) AS "nb_optin",
				SUM(' . _DB_PREFIX_ . 'customer.newsletter) AS "nb_newsletter"
				FROM ' . _DB_PREFIX_ . 'customer 
				WHERE ' . _DB_PREFIX_ . 'customer.active = 1
				AND ' . _DB_PREFIX_ . 'customer.deleted = 0';
		if( isset($periodID) && $periodID != "all" )
		{
			$monthStr = parent::getValidMonth( $this->period[$periodID]["month"] );
			$query .= " AND " . _DB_PREFIX_ . "customer.date_add >= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-00 00:00:00'";
			$query .= " AND " . _DB_PREFIX_ . "customer.date_add <= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-32 00:00:00'";
		}
		$this->lastQuery = $query;
		$this->lastStat = Db::getInstance()->executeS( $query );

		return $this->lastStat;
	}

	public function getClientByPeriod( $periodID = null )
	{
		$query = 'SELECT DISTINCT ' . _DB_PREFIX_ . 'customer.id_customer,
				count(' . _DB_PREFIX_ . 'customer.id_customer) AS "nb_user",
				SUM(' . _DB_PREFIX_ . 'customer.optin) AS "nb_optin",
				SUM(' . _DB_PREFIX_ . 'customer.newsletter) AS "nb_newsletter"
				FROM ' . _DB_PREFIX_ . 'customer 
				INNER JOIN ' . _DB_PREFIX_ . 'orders
				ON ' . _DB_PREFIX_ . 'customer.id_customer = ' . _DB_PREFIX_ . 'customer.id_customer 
				WHERE ' . _DB_PREFIX_ . 'customer.active = 1
				AND ' . _DB_PREFIX_ . 'customer.deleted = 0
				AND ' . _DB_PREFIX_ . 'orders.valid = 1
				AND ' . _DB_PREFIX_ . 'orders.total_paid_real != 0';
		if( isset($periodID) && $periodID != "all" )
		{
			$monthStr = parent::getValidMonth( $this->period[$periodID]["month"] );
			$query .= " AND " . _DB_PREFIX_ . "customer.date_add >= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-00 00:00:00'";
			$query .= " AND " . _DB_PREFIX_ . "customer.date_add <= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-32 00:00:00'";
		}
		// $query .= ' GROUP BY ' . _DB_PREFIX_ . 'customer.id_customer';
		$this->lastQuery = $query;
		$this->lastStat = Db::getInstance()->executeS( $query );

		return $this->lastStat;
	}

	private function displayStat()
	{
		global $smarty;
		parent::getActivityPeriod();
		$this->assignPostData();
		switch ( $this->lastPostData["statID"] )
		{
			case 1:
				$this->getNewUserByPeriod( $this->lastPostData["period"] );
				$this->displayInfo[] = array("message" =>	'<p>Ces statistiques reposent sur la totalité des utilisateurs présents en base, incluant les utilisateurs
											"supprimés" ou "désactivés".</p>',
											"class" => "alert-warning"
											);
				break;
			case 2:
				$this->getClientByPeriod( $this->lastPostData["period"] );
				break;
			case 0:
			default:
				$this->getNewActifUserByPeriod( $this->lastPostData["period"] );
				break;
		}
		if ( !empty($this->displayInfo) )
		{
			$smarty->assign( "admBkoStat_displayInfo", $this->displayInfo );
		}
		$this->assignGlobalVar();
		$smarty->assign( "admBkoStat_tplName", "usser" );
		$this->setTemplate( $this->path . $this->pathTPL . 'stats-index.tpl' );
		return true;
	}
}
<?php
/**
*2015 Bulko
*
*BkoStats module
*
*@author	Bulko (Golga)
*@copyright	Copyright (c) Bulko
*@license	Addons PrestaShop license
*
*AdminBkoStatsCAController tab for admin panel
*/

include_once 'AdminBkoStatsController.php';

class AdminBkoStatsCAController extends AdminBkoStatsController
{
	private $statOpt = array("Chiffre d'affaire", "Chiffre d'affaire par produit", "Chiffre d'affaire par catégorie");
	private $allowState = array(2, 3, 4, 5, 12, 13);
	private $displayInfo = array();

	public function __construct()
	{
		parent::__construct( $this->statOpt, $this->allowState );
		return true;
	}

	public function initContent()
	{
		parent::initContent();
		$this->displayStat();

		return true;
	}

	public function allowOrderStateClosur( )
	{
		$sql = "(";
		$length = count( $this->allowState ) - 1;
		foreach ($this->allowState as $key => $value)
		{
			$sql .= ' ' . _DB_PREFIX_ . 'orders.current_state = ' . $value;
			if ( $key != $length )
			{
				$sql .= ' OR';
			}
		}
		$sql .= ' ) AND ' . _DB_PREFIX_ . 'orders.valid = 1';

		return $sql;
	}

	public function getCAByPeriod( $periodID = null )
	{
		$query = 'SELECT count(' . _DB_PREFIX_ . 'orders.id_order) AS "nb_vente",
				SUM(' . _DB_PREFIX_ . 'orders.total_paid_tax_excl) AS "total_HT",
				SUM(' . _DB_PREFIX_ . 'orders.total_paid_tax_incl) AS "total_TTC",
				SUM(' . _DB_PREFIX_ . 'orders.total_shipping_tax_excl) AS "shipping_HT",
				SUM(' . _DB_PREFIX_ . 'orders.total_shipping_tax_incl) AS "shipping_TTC"
				FROM ' . _DB_PREFIX_ . 'orders';
				
		$query .= " WHERE " . $this->allowOrderStateClosur();
		if( isset($periodID) && $periodID != "all" )
		{
			$monthStr = parent::getValidMonth( $this->period[$periodID]["month"] );
			$query .= " AND " . _DB_PREFIX_ . "orders.date_add >= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-00 00:00:00'";
			$query .= " AND " . _DB_PREFIX_ . "orders.date_add <= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-32 00:00:00'";
		}
		$this->lastQuery = $query;
		$this->lastStat = Db::getInstance()->executeS( $query );

		return $this->lastStat;
	}

	public function getCAProductByPeriod( $periodID = null )
	{
		$query = 'SELECT 
				' . _DB_PREFIX_ . 'order_detail.product_id,
				' . _DB_PREFIX_ . 'order_detail.product_name,
				SUM(' . _DB_PREFIX_ . 'order_detail.product_quantity) AS "nb_produit",
				SUM(' . _DB_PREFIX_ . 'order_detail.total_price_tax_excl) AS "total_HT",
				SUM(' . _DB_PREFIX_ . 'order_detail.total_price_tax_incl) AS "total_TTC",
				' . _DB_PREFIX_ . 'order_detail.unit_price_tax_excl AS "unit_HT",
				' . _DB_PREFIX_ . 'order_detail.unit_price_tax_incl AS "unit_TTC"
				FROM ' . _DB_PREFIX_ . 'orders 
				LEFT JOIN ' . _DB_PREFIX_ . 'order_detail ON ' . _DB_PREFIX_ . 'orders.id_order = ' . _DB_PREFIX_ . 'order_detail.id_order';
		$query .= " WHERE " . $this->allowOrderStateClosur();
		if( isset($periodID) && $periodID != "all" )
		{
			$monthStr = parent::getValidMonth( $this->period[$periodID]["month"] );
			$query .= " AND " . _DB_PREFIX_ . "orders.date_add >= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-00 00:00:00'";
			$query .= " AND " . _DB_PREFIX_ . "orders.date_add <= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-32 00:00:00'";
		}
		$query .= ' GROUP BY ' . _DB_PREFIX_ . 'order_detail.product_id';
		$this->lastQuery = $query;
		$this->lastStat = Db::getInstance()->executeS( $query );

		return $this->lastStat;
	}

	public function getCACategoryByPeriod( $periodID = null )
	{
		$query = 'SELECT
				' . _DB_PREFIX_ . 'product.id_category_default,
				' . _DB_PREFIX_ . 'category_lang.name AS category_name,
				SUM(' . _DB_PREFIX_ . 'order_detail.product_quantity) AS "nb_produit",
				SUM(' . _DB_PREFIX_ . 'order_detail.total_price_tax_excl) AS "total_HT",
				SUM(' . _DB_PREFIX_ . 'order_detail.total_price_tax_incl) AS "total_TTC"
				FROM ' . _DB_PREFIX_ . 'orders 
				LEFT JOIN ' . _DB_PREFIX_ . 'order_detail ON ' . _DB_PREFIX_ . 'orders.id_order = ' . _DB_PREFIX_ . 'order_detail.id_order
				LEFT JOIN ' . _DB_PREFIX_ . 'product ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'order_detail.product_id
				LEFT JOIN ' . _DB_PREFIX_ . 'category_lang ON ' . _DB_PREFIX_ . 'category_lang.id_category = ' . _DB_PREFIX_ . 'product.id_category_default';
		$query .= " WHERE " . $this->allowOrderStateClosur();
		if( isset($periodID) && $periodID != "all" )
		{
			$monthStr = parent::getValidMonth( $this->period[$periodID]["month"] );
			$query .= " AND " . _DB_PREFIX_ . "orders.date_add >= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-00 00:00:00'";
			$query .= " AND " . _DB_PREFIX_ . "orders.date_add <= '" . $this->period[$periodID]["year"] . "-" . $monthStr . "-32 00:00:00'";
		}
		// $query .= ' GROUP BY ' . _DB_PREFIX_ . 'order_detail.product_id';
		$query .= ' GROUP BY ' . _DB_PREFIX_ . 'product.id_category_default';
		$this->lastQuery = $query;
		$this->lastStat = Db::getInstance()->executeS( $query );

		$lostVar = array();
		foreach ($this->lastStat as $key => $value)
		{
			if ( empty($value["id_category_default"]) || empty($value["category_name"]) )
			{
				foreach ($value as $k => $v) 
				{
					switch ($k)
					{
						case 'id_category_default':
							$lostVar[$k] = "Ø";
							break;
						case 'category_name':
							$lostVar[$k] = "[ Catégorie ou id introuvable ]";
							break;
						default:
							if ( isset($lostVar[$k]) )
							{
								$lostVar[$k] = $lostVar[$k] + $v;
							}
							else
							{
								$lostVar[$k] = $v;
							}
							break;
					}
				}
				unset($this->lastStat[$key]);
			}
		}
		if ( !empty($lostVar) )
		{
			$this->lastStat[ ] = $lostVar;
		}
		return $this->lastStat;
	}

	private function displayStat()
	{
		global $smarty;

		parent::getActivityPeriod();
		$this->assignPostData();
		switch ( $this->lastPostData["statID"] )
		{
			case 1:
				$this->getCAProductByPeriod( $this->lastPostData["period"] );
				$this->getTotalByPeriod( );
				break;
			case 2:
				$this->getCACategoryByPeriod( $this->lastPostData["period"] );
				$this->getTotalByPeriod( );

				$this->displayInfo[] = array("message" =>	'<p>- Ces statistiques reposent sur la "catégorie par défaut" de chaque produit, supprimer un produit,
															changer ou supprimer sa catégorie par défaut peut nuire à l\'intégrité des statistiques.</p>
															<p>- En cas de catégories/produits supprimés ou déplacés, une ligne "[ Catégorie ou id introuvable ]"
															peut apparaître.</p>',
											"class" => "alert-warning"
											);

				break;
			case 0:
			default:
				$this->getCAByPeriod( $this->lastPostData["period"] );
				break;
		}
		if ( !empty($this->displayInfo) )
		{
			$smarty->assign( "admBkoStat_displayInfo", $this->displayInfo );
		}
		$smarty->assign( "admBkoStat_tplName", "ca" );
		$this->assignGlobalVar();

		$this->setTemplate( $this->path . $this->pathTPL . 'stats-index.tpl' );
		return true;
	}
}
<?php
/**
*2015 Bulko
*
*BkoStats module
*
*@author	Bulko (Golga)
*@copyright	Copyright (c) Bulko
*@license	Addons PrestaShop license
*
*AdminBkoStatsController tab for admin panel
*/

class AdminBkoStatsController extends ModuleAdminController
{
	private $monthName = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
	private $statOpt;
	private $allowState;

	public $path;
	public $realPath;
	public $pathTPL;
	public $pathInclude;
	
	public $lastStat;
	public $totalStat;
	public $lastPeriodID;
	public $lastQuery;
	public $lastPostData;

	public function __construct( $statOpt, $allowState = null )
	{
		$this->bootstrap = true;
		$this->table = 'BkoStats';
		$this->className = 'BkoStats';
		$this->module = 'bkostats';
		$this->lang = true;
		$this->statOpt = $statOpt;
		$this->allowState = $allowState;
		$this->pathTPL = "/views/templates/admin/";
		$this->realPath = explode("controllers", __FILE__)[0];
		if ( $_SERVER['REMOTE_ADDR'] == "127.0.0.1" || $_SERVER['REMOTE_ADDR'] == "::1" )
		{
			$this->path = "../../../../";
			$this->pathInclude = $this->path . $this->pathTPL;
		}
		else
		{
			$this->path = "../../../../" . _MODULE_DIR_ . $this->module;
			$this->pathInclude = "../admin";
		}
		
		parent::__construct();
		return true;
	}

	public function initContent()
	{
		parent::initContent();
		$this->displayStat();

		return true;
	}

	public function initToolBar()
	{
		parent::initPageHeaderToolbar();

		return true;
	}

	public function getValidMonth($month)
	{
		if ( $month > 9 )
		{
			return $month;
		}
		else
		{
			return "0".$month;
		}
	}

	public function getActivityPeriod()
	{
		$query = 'SELECT date_add AS "pTime"
				FROM ' . _DB_PREFIX_ . 'orders
				ORDER BY id_order ASC
				LIMIT 1';
		$startIn = explode( "-", Db::getInstance()->executeS( $query )[0]["pTime"] );
		$query = 'SELECT date_add AS "pTime"
				FROM ' . _DB_PREFIX_ . 'orders
				ORDER BY id_order DESC
				LIMIT 1';
		$this->lastQuery = $query;
		$endIn = explode( "-", Db::getInstance()->executeS( $query )[0]["pTime"] );
		
		$periodRow = array(
			'row' => 0,
			'month' => intval( $startIn[1] ),
			'year' => intval( $startIn[0] ),
			'monthName' => $this->monthName[ intval( $startIn[1] ) ]
			);
		$this->period = array( 0 => $periodRow );

		while ( $periodRow["month"] != intval( $endIn[1] ) || $periodRow["year"] != intval( $endIn[0] )  )
		{
			$row = $periodRow["row"] + 1;
			$month = $periodRow["month"] + 1;
			$year = $periodRow["year"];
			if ( $month == 13 )
			{
				$periodRow["month"] = 1;
				$month = 1;
				$year ++;
				$periodRow["year"] = $year;
			}
			$periodRow = array(
				'row' => $row,
				'month' => $month,
				'year' => $year,
				'monthName' => $this->monthName[$month]
			);
			$this->period[$row] = $periodRow;
		}

		return $this->period;
	}


	public function assignPostData()
	{
		if ( !empty($_POST) )
		{
			$postdata = $_POST;
		}
		else
		{
			$postdata = array(
					"period" => "all",
					"statID" => 0
				);
		}
		$this->lastPostData = $postdata;

		return true;
	}

	public function getTotalByPeriod()
	{
		$total = array();
		foreach ($this->lastStat as $key => $value)
		{
			foreach ($value as $k => $v)
			{
				if ( isset($total[$k]) )
				{
					$total[$k] = $total[$k] + $v;
				}
				else
				{
					$total[$k] = $v;
				}
			}
		}
		$this->totalStat = $total;
		if ( empty($total) )
		{
			return false;
		}
		return $total;
	}

	public function assignGlobalVar()
	{
		global $smarty, $cookie;
		$smarty->assign( "admBkoStat_path", $this->path );
		$smarty->assign( "admBkoStat_pathTPL", $this->pathTPL );
		$smarty->assign( "admBkoStat_includePath", $this->pathInclude );
		$smarty->assign( "admBkoStat_monthName", $this->monthName );
		$smarty->assign( "admBkoStat_period", $this->period );
		$smarty->assign( "admBkoStat_statOpt", $this->statOpt );
		$smarty->assign( "admBkoStat_lastStat", $this->lastStat );
		$smarty->assign( "admBkoStat_lastPeriodID", $this->lastPeriodID );
		$smarty->assign( "admBkoStat_lastQuery", $this->lastQuery );
		$smarty->assign( "admBkoStat_postData", $this->lastPostData );
		$smarty->assign( "admBkoStat_CALink", "index.php?" . "controller=AdminBkoStatsCA&token=" . Tools::getAdminToken('AdminBkoStatsCAController'.intval(Tab::getIdFromClassName('AdminBkoStatsCAController')).intval($cookie->id_employee)));
		$smarty->assign( "admBkoStat_UsserLink", "index.php?" . "controller=AdminBkoStatsUsser&token=" . Tools::getAdminToken('AdminBkoStatsCAController'.intval(Tab::getIdFromClassName('AdminBkoStatsUsserController')).intval($cookie->id_employee)));
		if ( isset($_GET["debug"]) )
		{
			$smarty->assign( "admBkoStat_debug", "true" );
		}
		if ( !empty($this->totalStat) )
		{
			$smarty->assign( "admBkoStat_totalStat", $this->totalStat );
		}
		
		return true;
	}

	private function displayStat()
	{
		if ( isset($_GET["controller"]) && $_GET["controller"] == "AdminBkoStats" )
		{
			$token=Tools::getAdminToken('AdminBkoStatsCAController'.intval(Tab::getIdFromClassName('AdminBkoStatsCAController')).intval($params['cookie']->id_employee));
			header("location: index.php?" . "controller=AdminBkoStatsCA&token=" . $token);
			exit();
		}
		return true;
	}
}
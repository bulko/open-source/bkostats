<?php
/**
*2015 Bulko
*
*BkoStat module
*
*@author    Bulko (Golga)
*@copyright Copyright (c) Bulko
*@license   Addons PrestaShop license
*
*BkoStat main module
*/
class Bkostats extends Module
{
	public function __construct()
	{
		$this->name = 'bkostats';
		$this->tab = 'analytics_stats';
		$this->version = 0.3;
		
		$this->author = 'Bulko';
		$this->displayName = $this->l('Statistiques Tauziet');
		$this->description = $this->l('Afficher des données statistiques pour lafermedetauziet.fr');
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
		// $this->module_key = "";
 
		parent::__construct();
	}

	public function install()
	{
		if ( 
				!parent::install()
			&&	$this->registerHook('displayBackOfficeHeader')
			)
		{
			return false;
		}
		// root tab 
		$tab0 = new Tab();
		$tab0->class_name = 'AdminBkoStats';
		$tab0->module = 'bkostats';
		$tab0->id_parent = 0;
		foreach (Language::getLanguages(true) as $lang) 
		{
			$tab0->name[$lang['id_lang']] = 'Stat Tauziet';
		}
		$tab0->save();
		$rootID = $tab0->id;

		// CA tab
		$tab1 = new Tab();
		$tab1->class_name = 'AdminBkoStatsCA';
		$tab1->module = 'bkostats';
		$tab1->id_parent = $rootID;
		foreach (Language::getLanguages(true) as $lang) 
		{
			$tab1->name[$lang['id_lang']] = 'Chiffre d\'affaire';
		}
		$tab1->save();

		// Usser tab
		$tab2 = new Tab();
		$tab2->class_name = 'AdminBkoStatsUsser';
		$tab2->module = 'bkostats';
		$tab2->id_parent = $rootID;
		foreach (Language::getLanguages(true) as $lang) 
		{
			$tab2->name[$lang['id_lang']] = 'Utilisateur';
		}
		$tab2->save();
		return true;
	}

	public function hookDisplayBackOfficeHeader($params)
	{
		return '<style type="text/css">.icon-AdminBkoStats:before{ content: "";  }</style>"';
	}
 
	public function uninstall()
 	{
 	 	if ( !parent::uninstall() )
 	 	{
 	 		return false;
 	 	}
 	 	$tab_id = Tab::getIdFromClassName('AdminBkoStats');
		if ($tab_id) 
		{
			$tab = new Tab($tab_id);
			$tab->delete();
		}
		$tab_id = Tab::getIdFromClassName('AdminBkoStatsCA');
		if ($tab_id) 
		{
			$tab = new Tab($tab_id);
			$tab->delete();
		}
		$tab_id = Tab::getIdFromClassName('AdminBkoStatsUsser');
		if ($tab_id) 
		{
			$tab = new Tab($tab_id);
			$tab->delete();
		}
 	 	return true;
 	}

 	public function getContent()
	{
		$token=Tools::getAdminToken('AdminBkoStatsController'.intval(Tab::getIdFromClassName('AdminBkoStatsController')).intval($params['cookie']->id_employee));
		header("location: index.php?" . "controller=AdminBkoStats&token=" . $token);
		exit();
	}
}
?>
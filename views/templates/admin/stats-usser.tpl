	<thead>
		<tr>
			<th>N°</th>
			<th>Nombre total</th>
			<th>Nombre inscrit à la newsletter</th>
			<th>Nombre optin</th>
		</tr>
	</thead>
	{foreach from=$admBkoStat_lastStat item=stat key=k}
		<tbody>
			<tr>
				<td>{$k}</td>
				<td>{$stat.nb_user}</td>
				<td>{$stat.nb_newsletter}</td>
				<td>{$stat.nb_optin}</td>
			</tr>
		</tbody>
	{/foreach}
{include file="$admBkoStat_includePath/header.tpl"}

{include file="$admBkoStat_includePath/stat-form.tpl"}

{if isset($admBkoStat_displayInfo)}
	{include file="$admBkoStat_includePath/info.tpl"}
{/if}

{if isset($admBkoStat_tplName)}
	<div class="nobootstrap" style="padding-top:25px; padding-left:0; padding-right:0">
		<fieldset>
			<legend>
				{foreach from=$admBkoStat_period item=period key=k}
					{if $admBkoStat_postData.period == $k AND $admBkoStat_postData.period != "all" }
						{$period.monthName} - {$period.year}
					{/if}
				{/foreach}
				{foreach from=$admBkoStat_statOpt item=statOpt key=k}
					{if $admBkoStat_postData.statID == $k}{$statOpt}{/if}
				{/foreach}
			</legend>
			<table class="table tableDnD">
			{include file="$admBkoStat_includePath/stats-$admBkoStat_tplName.tpl"}
			</table>
		</fieldset>
	</div>
{/if}

{if isset($admBkoStat_debug) AND $admBkoStat_debug == "true"}
	{include file="$admBkoStat_includePath/debug.tpl"}
{/if}

{include file="$admBkoStat_includePath/footer.tpl"}

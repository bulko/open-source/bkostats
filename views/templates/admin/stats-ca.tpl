{if $admBkoStat_postData.statID == 0}
	<thead>
		<tr>
			<th>N°</th>
			<th>Nombre de commandes</th>
			<th>CA HT <i>(*frais de port inclus)</i></th>
			<th>CA TTC <i>(*frais de port inclus)</i></th>
			<th>CA HT <i>(*frais de port exclus)</i></th>
			<th>CA TTC <i>(*frais de port exclus)</i></th>
		</tr>
	</thead>
	{foreach from=$admBkoStat_lastStat item=stat key=k}
		<tbody>
			<tr>
				<td>{$k}</td>
				<td>{$stat.nb_vente}</td>
				<td>{$stat.total_HT}</td>
				<td>{$stat.total_TTC}</td>
				<td>{$stat.total_HT - $stat.shipping_HT}</td>
				<td>{$stat.total_TTC - $stat.shipping_TTC}</td>
			</tr>
		</tbody>
	{/foreach}
{elseif $admBkoStat_postData.statID == 1}
	<thead>
		<tr>
			<th>N°</th>
			<th>ID</th>
			<th>Nom du produit</th>
			<th>Nombre de ventes</th>
			<th>Total HT</th>
			<th>Total TTC</th>
			<th>Tarif unitaire HT</th>
			<th>Tarif unitaire TTC</th>
		</tr>
	</thead>
	{foreach from=$admBkoStat_lastStat item=stat key=k}
		<tbody>
			<tr>
				<td>{$k}</td>
				<td>{$stat.product_id}</td>
				<td>{$stat.product_name}</td>
				<td>{$stat.nb_produit}</td>
				<td>{$stat.total_HT}</td>
				<td>{$stat.total_TTC}</td>
				<td>{$stat.unit_HT}</td>
				<td>{$stat.unit_TTC}</td>
			</tr>
		</tbody>
	{/foreach}
	{if isset($admBkoStat_totalStat)}
		<tfoot>
			<tr>
				<td>{$k+1}</td>
				<td>Ø</td>
				<td>Total</td>
				<td>{$admBkoStat_totalStat.nb_produit}</td>
				<td>{$admBkoStat_totalStat.total_HT}</td>
				<td>{$admBkoStat_totalStat.total_TTC}</td>
				<td>{$admBkoStat_totalStat.unit_HT}</td>
				<td>{$admBkoStat_totalStat.unit_TTC}</td>
			</tr>
		</tfoot>
	{/if}
{elseif $admBkoStat_postData.statID == 2}
	<thead>
		<tr>
			<th>N°</th>
			<th>ID</th>
			<th>Nom de la catégorie</th>
			<th>Nombre de produit vendue</th>
			<th>Total HT</th>
			<th>Total TTC</th>
		</tr>
	</thead>
	{foreach from=$admBkoStat_lastStat item=stat key=k}
		<tbody>
			<tr>
				<td>{$k}</td>
				<td>{$stat.id_category_default}</td>
				<td>{$stat.category_name}</td>
				<td>{$stat.nb_produit}</td>
				<td>{$stat.total_HT}</td>
				<td>{$stat.total_TTC}</td>
			</tr>
		</tbody>
	{/foreach}
	{if isset($admBkoStat_totalStat)}
		<tfoot>
			<tr>
				<td>{$k+1}</td>
				<td>Ø</td>
				<td>Total</td>
				<td>{$admBkoStat_totalStat.nb_produit}</td>
				<td>{$admBkoStat_totalStat.total_HT}</td>
				<td>{$admBkoStat_totalStat.total_TTC}</td>
			</tr>
		</tfoot>
	{/if}
{/if}
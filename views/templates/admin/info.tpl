{foreach from=$admBkoStat_displayInfo item=info}
	<div class="alert {$info.class}">
		<button data-dismiss="alert" class="close" type="button">×</button>
		{$info.message}
	</div>
{/foreach}
<div class="panel">
	<form method="post" accept-charset="utf-8">
		<div class="panel-heading">
			<i class="icon-cogs"></i>
			Options
		</div>
		<div class="form-wrapper">
			<label for="period"> {l s='Choisir ma période'} </label>
			<select name="period">
				<option value="all" {if $admBkoStat_postData.period == "all"}selected {/if}> {l s="Toutes les periodes"} </option>
				{foreach from=$admBkoStat_period item=period key=k}
					<option value="{$k}" {if $admBkoStat_postData.period == $k AND $admBkoStat_postData.period != "all" }selected {/if}> {$period.monthName} - {$period.year}</option>
				{/foreach}
			</select>
			<label for="period"> {l s='Choisir la statistique à afficher'} </label>
			<select name="statID">
				{foreach from=$admBkoStat_statOpt item=statOpt key=k}
					<option value="{$k}" {if $admBkoStat_postData.statID == $k}selected {/if}>{$statOpt}</option>
				{/foreach}
			</select>
			<div class="panel-footer">
				<button class="btn btn-default pull-right">
					<i class="process-icon-refresh"></i>
					Générer
				</button>
			</div>
		</div>
	</form>
</div>